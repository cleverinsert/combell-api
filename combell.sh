#!/bin/bash

# https://my.combell.nl/nl/settings/api

# https://certbot.eff.org/docs/using.html#pre-and-post-validation-hooks

# CERTBOT_DOMAIN: The domain being authenticated
# CERTBOT_VALIDATION: The validation string (HTTP-01 and DNS-01 only)
# CERTBOT_TOKEN: Resource name part of the HTTP-01 challenge (HTTP-01 only)

################################################################################

if [ ! -x "$(command -v jq)" ]; then
  echo "Error: jq is not installed, required for parsing json: https://stedolan.github.io/jq/download/"
  exit 1
fi

if [ -z ${COMBELLAPI_CLIENT_DOMAIN} ] || [ -z ${COMBELLAPI_SECRET} ] || [ -z ${COMBELLAPI_KEY} ]; then
  echo "Configuration required. Set these environment variables: COMBELLAPI_CLIENT_DOMAIN, COMBELLAPI_SECRET, COMBELLAPI_KEY"
  exit 126
fi

if [ -z ${CERTBOT_VALIDATION} ]; then
  echo "Certbot didn't set CERTBOT_VALIDATION. Bye."
  exit 126
fi

function DO_CONCAT {
  CONTENT_HASH=""

  if [ -n "${CONTENT}" ]; then
    CONTENT_HASH=$(echo -n "${CONTENT}" | openssl dgst -md5 -binary | base64)
  fi

  UNIX_TIMESTAMP=$(date +%s)
  NONCE=$(openssl rand -base64 32)

  CONCAT=$(printf %s%s%s%s%s%s \
    ${COMBELLAPI_KEY} \
    $(echo ${REQUEST_METHOD} | tr '[:upper:]' '[:lower:]') \
    $(echo ${PATH_QUERYSTRING} | sed 's/\//%2F/g' | sed 's/?/%3F/g' | sed 's/=/%3D/g' ) \
    ${UNIX_TIMESTAMP} \
    ${NONCE} \
    ${CONTENT_HASH})
}

function DO_CURL {

  HMAC=$(echo -n ${CONCAT} | openssl dgst -sha256 -hmac ${COMBELLAPI_SECRET} -binary | base64)

  case ${REQUEST_METHOD} in
    GET)
      JSON=$(curl --trace-ascii traceget.txt --fail --silent --show-error \
        --header "Authorization: hmac $COMBELLAPI_KEY:$HMAC:$NONCE:$UNIX_TIMESTAMP" \
        "$HOST$PATH_QUERYSTRING")
    ;;
    POST)
      JSON=$(curl --trace-ascii tracepost.txt --fail --silent --show-error \
        --header "Authorization: hmac $COMBELLAPI_KEY:$HMAC:$NONCE:$UNIX_TIMESTAMP" \
        --header "Content-Type: application/json" \
        --data-binary "${CONTENT}" \
        "$HOST$PATH_QUERYSTRING")
    ;;
    DELETE)
      JSON=$(curl --trace-ascii tracedelete.txt --fail --silent --show-error \
        -X ${REQUEST_METHOD} \
        --header "Authorization: hmac $COMBELLAPI_KEY:$HMAC:$NONCE:$UNIX_TIMESTAMP" \
        "$HOST$PATH_QUERYSTRING")
    ;;
  esac
}


HOST="https://api.combell.nl"
TXT_NAME="_acme-challenge"

REQUEST_METHOD="GET"
PATH_QUERYSTRING="/v2/dns/${COMBELLAPI_CLIENT_DOMAIN}/records?take=100"
CONTENT=""

DO_CONCAT
DO_CURL

# Find existing records, delete them. If none were found, create one.
IDS=$(printf %s ${JSON} | jq -r --arg TXT_NAME ${TXT_NAME} '.[] | select(.type == "TXT" and .record_name == $TXT_NAME) | .id')
for ID in ${IDS}; do
  REQUEST_METHOD="DELETE"
  PATH_QUERYSTRING="/v2/dns/${COMBELLAPI_CLIENT_DOMAIN}/records/${ID}"
  DO_CONCAT
  DO_CURL
  echo "Deleted record with internal ID ${ID}"
done;


if [ -n "${IDS}" ]; then
  echo "Exiting, record deleted, not adding a new one. Bye."
  exit 0
fi

echo "Adding new record"

# Create new TXT record
REQUEST_METHOD="POST"
PATH_QUERYSTRING="/v2/dns/${COMBELLAPI_CLIENT_DOMAIN}/records"
CONTENT=$(printf '{"type": "TXT", "record_name": "%s", "ttl": 60, "content": "%s"}' ${TXT_NAME} "${CERTBOT_VALIDATION}" | jq)

DO_CONCAT
DO_CURL

sleep 25
